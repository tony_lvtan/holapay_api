﻿namespace Hola.Core.Model
{
    public class JsonResponseModel
    {
        public int Status { set; get; }
        public object Data { set; get; }
        public string Message { set; get; }
    }
}