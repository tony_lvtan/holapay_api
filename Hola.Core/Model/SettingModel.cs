﻿namespace Hola.Core.Model
{
    public class SettingModel
    {
        public string Provider { set; get; }
        public string Connection { set; get; }
    }
    
}