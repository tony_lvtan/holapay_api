﻿using System;
using System.Collections.Generic;
using System.Data;
using Hola.Core.Model;
using Hola.Core.Provider;
using Hola.Core.Utils;

namespace Hola.Core.Helper
{
    public static class DatabaseHelper
    {
        public static DataTable ExcuteSql(string sql,SettingModel setting)
        {
            DataTable dt = new DataTable();
            try
            {
                var connection = Providers.GetConnection(setting);
                var command = connection.CreateCommand();
                command.CommandText = sql;
                command.CommandType = CommandType.Text;
                dt.Load(command.ExecuteReader());
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }
            finally
            {
                Providers.Close();
            }

            return dt;
        }

        public static int ExcuteNonQuery(string sql, SettingModel setting)
        {
            try
            {
                var connection = Providers.GetConnection(setting);
                var command = connection.CreateCommand();
                command.CommandText = sql;
                command.CommandType = CommandType.Text;
                return command.ExecuteNonQuery();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                return 0;
            }
            finally
            {
                Providers.Close();
            }
        }

        #region Excute and Convert

        public static List<T> ExcuteQueryToList<T>(string sql, SettingModel setting)
        {
            DataTable dt = ExcuteSql(sql, setting);
            return Converter.DataTableToList<T>(dt);
        }

        public static List<Dictionary<string, object>> ExcuteQueryToDict(string sql, SettingModel setting)
        {
            DataTable dataTable = ExcuteSql(sql, setting);
            return Converter.ParseTableToDictationary(dataTable);
        }
        #endregion
    }
}