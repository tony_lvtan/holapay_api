﻿using System.Collections.Generic;
using System.Linq;
using Hola.Api.Config.Model;
using Hola.Core.Helper;
using Hola.Core.Model;
using Hola.Core.Model.DBModel.cmn;
using Microsoft.Extensions.Options;

namespace Hola.Api.Config.Service
{
    public class LanguageService
    {
        private readonly IOptions<SettingModel> _options;

        public LanguageService(IOptions<SettingModel> setting)
        {
            _options = setting;
        }
        public Dictionary<string,string> GetLanguageBycode(string iso)
        {
            SettingModel setting = new SettingModel()
            {
                Connection = _options.Value.Connection,
                Provider = _options.Value.Provider
            };
            string sql =
                string.Format(
                    "SELECT T.* FROM cmn.LanguageText AS T INNER JOIN cmn.Language AS L ON T.LanguageId = L.id where (L.TwoLetterISOLanguageName = '{0}' or L.ThreeLetterISOLanguageName = '{0}')",
                    iso);
            return DatabaseHelper.ExcuteQueryToList<LanguageTextModel>(sql, setting)
                .ToDictionary(key => key.LanguageKey, val => val.LanguageText);
        }
    }
}