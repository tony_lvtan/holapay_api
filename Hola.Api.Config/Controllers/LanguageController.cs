﻿using System.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Hola.Api.Config.Service;
using Hola.Core.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Hola.Api.Config.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class LanguageController : ControllerBase
    {
        // GET
        private readonly ILogger<LanguageController> _logger;
        private readonly IOptions<SettingModel> _config;

        public LanguageController(ILogger<LanguageController> logger,IOptions<SettingModel> config)
        {
            _logger = logger;
            _config = config;
        }

        [HttpGet]
        public JsonResponseModel Text(string iso)
        {
            _logger.LogInformation("param : " + iso);
            JsonResponseModel response = new JsonResponseModel();
            LanguageService _service = new LanguageService(_config);
            var data = _service.GetLanguageBycode(iso);
            if (data.Any())
            {
                response.Status = 200;
                response.Data = data;
            }
            else
            {
                response.Status = 100;
                response.Message = "Data not found";
            }
            _logger.LogInformation(JsonSerializer.Serialize(response));
            return response;
        }
    }
}