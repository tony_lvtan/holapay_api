﻿namespace Hola.Api.Config.Model
{
    public class LanguageResponseModel
    {
        public string ko { set; get; }
        public string ok { set; get; }
        public string permission_photos { set; get; }
        public string do_not_allow { set; get; }
    }
}